# What?

CLI tool for Google Admin

## Examples

### Users

#### List

    $ gadmin users list example.com
    member1@example.com
    member2@example.com

#### User aliases

##### List

    $ gadmin users alias list member1@example.com
    my-alias1@example.com

##### Add

    $ gadmin users alias insert member1@example.com my-alias11@example.com
    $ gadmin users alias list member1@example.com
    my-alias1@example.com
    my-alias11@example.com

##### Delete

    $ gadmin users alias delete member1@example.com my-alias11@example.com
    $ gadmin users alias list member1@example.com
    my-alias1@example.com

### Groups

#### Create

    $ gadmin groups create my-group@example.com

#### List

    $ gadmin groups list example.com
    my-group@example.com

#### Rename

    $ gadmin groups rename my-group@example.com renamed@example.com
    $ gadmin groups list example.com
    renamed@example.com
    $ gadmin groups rename renamed@example.com my-group@example.com
    $ gadmin groups list example.com
    my-group@example.com

#### Count members

    $ gadmin groups count example.com
    my-group@example.com 2

#### Delete

    $ gadmin groups delete foo@example.com

#### Delete groups with no members

    $ gadmin groups count example.com \
    | awk '$2=="0" {print $1}' \
    | xargs -rp -n1 gadmin groups delete

#### Group aliases

##### List

    $ gadmin groups aliases list my-group@example.com
    test1@example.com
    test2@example.com

##### Add

    $ gadmin groups aliases insert my-group@example.com testalias@example.com
    $ gadmin groups aliases list my-group@example.com
    test1@example.com
    test2@example.com
    testalias@example.com

##### Delete

    $ gadmin groups aliases delete my-group@example.com testalias@example.com
    $ gadmin groups aliases list my-group@example.com
    test1@example.com
    test2@example.com

#### Members

##### List

    $ gadmin groups members list my-group@example.com
    member1@example.com
    member2@example.com

##### Count

    $ gadmin groups members count my-group@example.com
    2
