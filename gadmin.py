#!/usr/bin/env python3
import click
import contextlib
import httplib2
import json
import oauth2client
import oauth2client.file
import webbrowser

from apiclient.discovery import build
from apiclient.errors import HttpError


class propdict(dict):

    def __getattr__(self, attr):
        try:
            return self.__getitem__(attr)
        except KeyError:
            raise AttributeError(attr)


def pprint(o):
    print(json.dumps(o, sort_keys=True, indent=4))


def get_directory(ctx):
    scope = [
        "https://www.googleapis.com/auth/admin.directory.user",
        "https://www.googleapis.com/auth/admin.directory.user.readonly",
        "https://www.googleapis.com/auth/admin.directory.group",
        "https://www.googleapis.com/auth/admin.directory.group.readonly",
    ]
    http = oauth_flow(scope)
    return build("admin", "directory_v1", http=http)


def paginated_execute(key, f):
    results = []
    nextPageToken = None
    while True:
        r = f(pageToken=nextPageToken)
        r = r.execute()
        results.extend(r[key])
        try:
            nextPageToken = r["nextPageToken"]
        except LookupError:
            break
    return results


def execute(key, f):
    results = []
    r = f().execute()
    return r[key]


@click.group()
@click.pass_context
def services(ctx):
    ctx.obj = propdict()
    ctx.obj["scope"] = []
    ctx.obj["directory"] = lambda: get_directory(ctx)


@services.group()
def users():
    pass


@users.command("list")
@click.argument("domain")
@click.pass_context
def users_list(ctx, domain):
    results = paginated_execute(
        "users",
        lambda pageToken:
        ctx.obj.directory().users().list(
            domain=domain,
            pageToken=pageToken,
        )
    )
    for o in results:
        print(o["primaryEmail"])
    return


@users.group("aliases")
def users_aliases():
    pass


@users_aliases.command("list")
@click.argument("userkey")
@click.pass_context
def users_aliases_list(ctx, userkey):
    results = execute(
        "aliases",
        lambda:
        ctx.obj.directory().users().aliases().list(
            userKey=userkey,
        )
    )
    for o in results:
        print(o["alias"])
    return


@users_aliases.command("insert")
@click.argument("userkey")
@click.argument("alias")
@click.pass_context
def users_aliases_insert(ctx, userkey, alias):
    r = (
        ctx.obj.directory().users().aliases()
        .insert(userKey=userkey, body={"alias": alias})
    )
    r.execute()
    return


@users_aliases.command("delete")
@click.argument("userkey")
@click.argument("alias")
@click.pass_context
def users_aliases_delete(ctx, userkey, alias):
    r = (
        ctx.obj.directory().users().aliases()
        .delete(userKey=userkey, alias=alias)
    )
    r.execute()
    return


@services.group()
def groups():
    pass


@groups.command("list")
@click.argument("domain")
@click.pass_context
def groups_list(ctx, domain):
    results = paginated_execute(
        "groups",
        lambda pageToken:
        ctx.obj.directory().groups().list(
            domain=domain,
            pageToken=pageToken,
        )
    )
    for o in results:
        print(o["email"])
    return


@groups.command("count")
@click.argument("domain")
@click.pass_context
def groups_count(ctx, domain):
    results = paginated_execute(
        "groups",
        lambda pageToken:
        ctx.obj.directory().groups().list(
            domain=domain,
            pageToken=pageToken,
        )
    )
    for o in results:
        print(o["email"], o["directMembersCount"])
    return


@groups.command("insert")
@click.argument("groupkey")
@click.pass_context
def groups_insert(ctx, groupkey):
    ctx.obj.directory().groups().insert(
        body={"email": groupkey}
    ).execute()
    return


@groups.command("rename")
@click.argument("groupkey")
@click.argument("newkey")
@click.pass_context
def groups_rename(ctx, groupkey, newkey):
    assert groupkey != newkey
    ctx.obj.directory().groups().update(
        groupKey=groupkey, body={"email": newkey},
    ).execute()
    ctx.obj.directory().groups().aliases().delete(
        groupKey=newkey, alias=groupkey
    ).execute()
    return


@groups.command("delete")
@click.argument("groupkey")
@click.pass_context
def groups_delete(ctx, groupkey):
    r = ctx.obj.directory().groups().delete(
        groupKey=groupkey
    )
    r = r.execute()
    return


@groups.command("members")
@click.argument("action", default="list",
                type=click.Choice(["list", "count"]))
@click.argument("groupkey")
@click.pass_context
def groups_members(ctx, action, groupkey):
    results = paginated_execute(
        "members",
        lambda pageToken:
        ctx.obj.directory().members().list(
            groupKey=groupkey,
            pageToken=pageToken,
        )
    )
    if action == "list":
        for o in results:
            print(o["email"])
    elif action == "count":
        print(len(results))
    else:
        raise AssertionError()
    return


@groups.command("aliases")
@click.argument("action", default="list",
                type=click.Choice(["list", "insert", "delete"]))
@click.argument("groupkey")
@click.argument("alias", required=False)
@click.pass_context
def groups_aliases(ctx, action, groupkey, alias):
    if action == "list":
        if alias:
            raise TypeError(alias)
        r = ctx.obj.directory().groups().aliases().list(
            groupKey=groupkey
        )
        results = r.execute()["aliases"]
        for o in results:
            print(o["alias"])
        return
    elif action == "insert":
        r = (
            ctx.obj.directory().groups().aliases()
            .insert(groupKey=groupkey, body={"alias": alias})
        )
    elif action == "delete":
        r = (
            ctx.obj.directory().groups().aliases()
            .delete(groupKey=groupkey, alias=alias)
        )
    else:
        raise AssertionError()
    r.execute()


def oauth_flow(scope):
    flow = oauth2client.client.flow_from_clientsecrets(
        "client_secrets.json", scope=scope,
        redirect_uri="urn:ietf:wg:oauth:2.0:oob",
    )
    storage = oauth2client.file.Storage("credentials.dat")
    credentials = storage.get()
    if not credentials or credentials.invalid:
        auth_uri = flow.step1_get_authorize_url()
        webbrowser.open_new(auth_uri)
        auth_code = input("code: ")
        credentials = flow.step2_exchange(auth_code)
        storage.put(credentials)
    return credentials.authorize(httplib2.Http())


def main():
    services()


if __name__ == "__main__":
    main()
