#!/usr/bin/env python3
from setuptools import setup


with open("requirements.txt") as f:
    install_requires = filter(None, f.readlines())


setup(
    name="gadmin",
    version="0.1",
    py_modules=["gadmin"],
    install_requires=install_requires,
    entry_points={
        "console_scripts": [
            "gadmin=gadmin:main",
        ],
    },
)
