google-api-python-client==1.4.*
httplib2==0.9.*
oauth2client==1.5.*
rsa==3.2.*
simplejson==3.8.*
six==1.10.*
click==4.*
